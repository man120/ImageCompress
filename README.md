#图片缩放的相关处理

***
最近自己做的点东西上用到了图片的处理，不过没有找到合适的图片处理相关算法，倒是找到了几个获取缩略图的算法。代码不长，简单看了看原理也比较简单（其实不是简单是C#的封装的太多了，所以显得比较简单）。既然没有相关的统一调用的代码，索性，我来整理一套相关的处理代码然后公开出来吧。
***

##图片处理的相关分析
***
其实我要用的图片处理非常简单，就是简单的压缩一下图片。然后把图片设置到对应大小而已。但是对于设置的方式，C#基础的函数中使用的Rect范围设置。这个范围设置计算起来可能相对头疼一些。所以我在考虑要不要让调用者在忽略Rect这个参数的情况愉快的调用接口呢？于是乎，我定义了几个图片缩放类型（Full、Zoom、Overflow、Original）。然后默认的让原始图片的中心点与输出图片的中心点对齐。然后根据缩放类型来对图片进行不同的缩放模式。
***

###缩放模式
1. Full：让图片放弃原来的比例，直接按照目标数据进行拉伸
2. Zoom：让图片保持原有比例，并且保证原始图片在目标图片的正中央，没有图片的位置保留米白色
3. Original：让图片保持原来的大小，输出图片类似于裁剪中间位置某个范围的图片
4. Overflow：让图片保持原来的比例，并且保证输目标是被图片填满的不留空白

###调用的范例
***
ImageHelper.GetInstance().ImageCompress(InputFile, Path.Combine(OutPutPath, item_file), new System.Drawing.Size(500, 300), ImageZoomType.Full, 50);
***

###样品展示
原始图片

![原始图片](http://ww1.sinaimg.cn/large/72f96cbajw1f82a4s38b8j20go0ci0wg.jpg)

Full 300 * 500

![Full 300 * 500](http://ww1.sinaimg.cn/large/72f96cbajw1f82a6f21mpj208c0dwglw.jpg)

Full 500 * 300

![Full 500 * 300](http://ww1.sinaimg.cn/large/72f96cbajw1f82a8w84c1j20dw08caac.jpg)

Original 300 * 500

![Original 300 * 500](http://ww3.sinaimg.cn/large/72f96cbajw1f82a91wznfj208c0dwglw.jpg)

Original 500 * 300

![Original 500 * 300](http://ww2.sinaimg.cn/large/72f96cbajw1f82a9bcc7xj20dw08caab.jpg)

Overflow 300 * 500

![Overflow 300 * 500](http://ww2.sinaimg.cn/large/72f96cbajw1f82a9fn4k5j208c0dwmxc.jpg)

Overflow 500 * 300

![Overflow 500 * 300](http://ww1.sinaimg.cn/large/72f96cbajw1f82a9kz3ygj20dw08cglx.jpg)

Zoom 300 * 500

![Zoom 300 * 500](http://ww1.sinaimg.cn/large/72f96cbajw1f82a9peidpj208c0dwaa4.jpg)

Zoom 500 * 300

![Zoom 500 * 300](http://ww1.sinaimg.cn/large/72f96cbajw1f82a9te7e8j20dw08cq35.jpg)


##存在的不足
1. 压缩没有独到的技术
2. 目前只支持JPEG格式的图片
3. 部分图片进行缩放之后会留下一个1像素浅浅的边

##后续的内容
***
图片处理不光是缩放，还有其他的很多东西比较实用（截图、压缩、按高缩放、按宽缩放、添加水印、添加隐藏水印），我准备把图片常用的相关处理。整理成一个帮助类，方便迁入到各个项目中去。
所以我建立了一个专门的图片处理的开源项目方便后续功能加入，地址如下：http://git.oschina.net/anxin1225/ImageCompress
***
