﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageCompress
{
    public struct AnchorPoint
    {
        //public AnchorPoint() : this(0, 0) { }
        public AnchorPoint(double x, double y)
        {
            X = x;
            Y = y;
        }

        public double X, Y;
    }

    public enum ImageZoomType
    {
        Full,
        Zoom,
        Overflow,
        Original,
    }

    public delegate void ImageHandle(Image image, Bitmap bitmap);

    public class ImageHelper
    {
        private ImageHelper() { }

        private static ImageHelper _image_helper = null;
        private Dictionary<ImageZoomType, ImageHandle> _name_to_handle = new Dictionary<ImageZoomType, ImageHandle>();

        public static ImageHelper GetInstance()
        {
            if (_image_helper == null)
            {
                _image_helper = new ImageHelper();

                //填满
                _image_helper._name_to_handle[ImageZoomType.Full] = (image, bitmap) =>
                {
                    _image_helper.DrawImage(image, bitmap, new Rectangle(new Point(0, 0), bitmap.Size));
                };

                //原始
                _image_helper._name_to_handle[ImageZoomType.Original] = (image, bitmap) =>
                {
                    _image_helper.DrawImage(image, bitmap, 1, new AnchorPoint(0.5, 0.5), new AnchorPoint(0.5, 0.5));
                };

                //溢出
                _image_helper._name_to_handle[ImageZoomType.Overflow] = (image, bitmap) =>
                {
                    float proportion_x = (float)bitmap.Width / image.Width;
                    float proportion_y = (float)bitmap.Height / image.Height;
                    _image_helper.DrawImage(image, bitmap, proportion_x > proportion_y ? proportion_x : proportion_y, new AnchorPoint(0.5, 0.5), new AnchorPoint(0.5, 0.5));
                };

                //缩放
                _image_helper._name_to_handle[ImageZoomType.Zoom] = (image, bitmap) =>
                {
                    float proportion_x = (float)bitmap.Width / image.Width;
                    float proportion_y = (float)bitmap.Height / image.Height;
                    _image_helper.DrawImage(image, bitmap, proportion_x < proportion_y ? proportion_x : proportion_y, new AnchorPoint(0.5, 0.5), new AnchorPoint(0.5, 0.5));
                };
            }

            return _image_helper;
        }

        /// <summary>
        /// 图片缩放
        /// </summary>
        /// <param name="source"></param>
        /// <param name="output"></param>
        /// <param name="ztype"></param>
        /// <returns></returns>
        public bool ImageZoom(Bitmap source, Bitmap output, ImageZoomType ztype)
        {
            bool success = false;
            if (success = _name_to_handle.ContainsKey(ztype))
            {
                _name_to_handle[ztype](source, output);
            }

            return success;
        }

        public Image LoadImage(string source_path)
        {
            Image image = null;
            while (true)
            {
                if (!File.Exists(source_path))
                    break;

                try
                {
                    image = Image.FromFile(source_path);
                }
                catch
                {
                    break;
                }

                break;
            }

            return image;
        }

        /// <summary>
        /// 将BitMap保存到磁盘上
        /// </summary>
        /// <param name="image">需要保存的图片</param>
        /// <param name="save_path">保存的路径</param>
        /// <param name="format">保存格式</param>
        /// <param name="flag">保存质量</param>
        /// <returns></returns>
        public bool SaveImage(Bitmap image, string save_path, ImageFormat format, int flag)
        {
            //以下代码为保存图片时，设置压缩质量  
            EncoderParameters ep = new EncoderParameters();
            long[] qy = new long[1];
            qy[0] = flag;//设置压缩的比例1-100  
            EncoderParameter eParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, qy);
            ep.Param[0] = eParam;
            try
            {
                ImageCodecInfo[] arrayICI = ImageCodecInfo.GetImageEncoders();
                ImageCodecInfo jpegICIinfo = null;
                for (int x = 0; x < arrayICI.Length; x++)
                {
                    if (arrayICI[x].FormatDescription.Equals("JPEG"))
                    {
                        jpegICIinfo = arrayICI[x];
                        break;
                    }
                }
                if (jpegICIinfo != null)
                {
                    image.Save(save_path, jpegICIinfo, ep);//dFile是压缩后的新路径  
                }
                else
                {
                    image.Save(save_path, format);
                }
                return true;
            }
            catch
            {
                return false;
            }
            finally
            {
                image.Dispose();
            }
        }

        /// <summary>
        /// 画图
        /// </summary>
        /// <param name="source">原始图像</param>
        /// <param name="output">输出图像</param>
        /// <param name="souce_scale">原始图像的缩放值</param>
        /// <param name="souce_anchor"></param>
        /// <param name="graphics_anchor"></param>
        public void DrawImage(Image source, Bitmap output, float souce_scale, AnchorPoint souce_anchor, AnchorPoint graphics_anchor)
        {
            DrawImage(source, output, souce_scale, souce_anchor, new Point((int)(output.Width * graphics_anchor.X), (int)(output.Height * graphics_anchor.Y)));
        }

        /// <summary>
        /// 画图
        /// </summary>
        /// <param name="source">原始图像</param>
        /// <param name="output">输出凸显</param>
        /// <param name="source_scale">图像的所放值</param>
        /// <param name="source_anchor">源图像锚点</param>
        /// <param name="souce_point">图像位置</param>
        public void DrawImage(Image source, Bitmap output, float source_scale, AnchorPoint source_anchor, Point souce_point)
        {
            var pic_po = new Point((int)(souce_point.X - source.Size.Width * source_scale * source_anchor.X), (int)(souce_point.Y - source.Size.Height * source_scale * source_anchor.Y));
            DrawImage(source, output, new Rectangle(pic_po, new Size((int)(source.Width * source_scale), (int)(source.Height * source_scale))));
        }

        /// <summary>
        /// 画图
        /// </summary>
        /// <param name="source"></param>
        /// <param name="output"></param>
        /// <param name="rect"></param>
        public void DrawImage(Image source, Bitmap output, Rectangle rect)
        {
            Graphics g = Graphics.FromImage(output);
            g.Clear(Color.WhiteSmoke);
            g.CompositingQuality = CompositingQuality.HighQuality;
            g.SmoothingMode = SmoothingMode.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.DrawImage(source, rect, 0, 0, source.Width, source.Height, GraphicsUnit.Pixel);
            g.Dispose();
        }
    }
}
