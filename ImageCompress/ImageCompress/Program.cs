﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageCompress
{
    class Program
    {
        const string OutPutPath = "OutPut";
        const string InputFile = "../../../Input.jpg";

        static void Main(string[] args)
        {
            if (!Directory.Exists(OutPutPath))
            {
                Directory.CreateDirectory(OutPutPath);
            }
            else
            {
                foreach (var item in Directory.GetFiles(OutPutPath))
                {
                    File.Delete(item);
                }
            }

            //foreach (var item in Enum.GetNames(typeof(ImageZoomType)))
            //{
            //    string item_file = string.Format("{0}_Width.jpg", item);
            //    Console.WriteLine("ImageCompress " + item_file);
            //    ImageHelper.GetInstance().ImageCompress(InputFile, Path.Combine(OutPutPath, item_file), new System.Drawing.Size(500, 300), (ImageZoomType)Enum.Parse(typeof(ImageZoomType), item), 50);
            //}

            //foreach (var item in Enum.GetNames(typeof(ImageZoomType)))
            //{
            //    string item_file = string.Format("{0}_Height.jpg", item);
            //    Console.WriteLine("ImageCompress " + item_file);
            //    ImageHelper.GetInstance().ImageCompress(InputFile, Path.Combine(OutPutPath, item_file), new System.Drawing.Size(300, 500), (ImageZoomType)Enum.Parse(typeof(ImageZoomType), item), 50);
            //}

            Console.WriteLine("ImageCompress Finsh");

            System.Diagnostics.Process.Start(OutPutPath);
        }
    }
}
