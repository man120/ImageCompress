﻿namespace ImageHandle
{
    partial class Form_Main
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Main));
            this.groupBox_tool = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.button_open = new System.Windows.Forms.Button();
            this.button_save = new System.Windows.Forms.Button();
            this.comboBox_tools = new System.Windows.Forms.ComboBox();
            this.groupBox_pic = new System.Windows.Forms.GroupBox();
            this.pictureBox_image = new System.Windows.Forms.PictureBox();
            this.groupBox_tools_item = new System.Windows.Forms.GroupBox();
            this.groupBox_tool.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.groupBox_pic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_image)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox_tool
            // 
            this.groupBox_tool.Controls.Add(this.flowLayoutPanel1);
            this.groupBox_tool.Location = new System.Drawing.Point(12, 12);
            this.groupBox_tool.Name = "groupBox_tool";
            this.groupBox_tool.Size = new System.Drawing.Size(200, 112);
            this.groupBox_tool.TabIndex = 0;
            this.groupBox_tool.TabStop = false;
            this.groupBox_tool.Text = "工 具";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.button_open);
            this.flowLayoutPanel1.Controls.Add(this.button_save);
            this.flowLayoutPanel1.Controls.Add(this.comboBox_tools);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 17);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(194, 92);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // button_open
            // 
            this.button_open.Location = new System.Drawing.Point(3, 3);
            this.button_open.Name = "button_open";
            this.button_open.Size = new System.Drawing.Size(188, 23);
            this.button_open.TabIndex = 0;
            this.button_open.Text = "打 开";
            this.button_open.UseVisualStyleBackColor = true;
            this.button_open.Click += new System.EventHandler(this.button_open_Click);
            // 
            // button_save
            // 
            this.button_save.Location = new System.Drawing.Point(3, 32);
            this.button_save.Name = "button_save";
            this.button_save.Size = new System.Drawing.Size(188, 23);
            this.button_save.TabIndex = 2;
            this.button_save.Text = "保 存";
            this.button_save.UseVisualStyleBackColor = true;
            this.button_save.Click += new System.EventHandler(this.button_save_Click);
            // 
            // comboBox_tools
            // 
            this.comboBox_tools.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_tools.FormattingEnabled = true;
            this.comboBox_tools.Location = new System.Drawing.Point(3, 61);
            this.comboBox_tools.Name = "comboBox_tools";
            this.comboBox_tools.Size = new System.Drawing.Size(188, 20);
            this.comboBox_tools.TabIndex = 1;
            this.comboBox_tools.SelectedIndexChanged += new System.EventHandler(this.comboBox_tools_SelectedIndexChanged);
            // 
            // groupBox_pic
            // 
            this.groupBox_pic.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_pic.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox_pic.Controls.Add(this.pictureBox_image);
            this.groupBox_pic.Location = new System.Drawing.Point(218, 12);
            this.groupBox_pic.Name = "groupBox_pic";
            this.groupBox_pic.Size = new System.Drawing.Size(554, 537);
            this.groupBox_pic.TabIndex = 0;
            this.groupBox_pic.TabStop = false;
            // 
            // pictureBox_image
            // 
            this.pictureBox_image.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox_image.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox_image.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox_image.BackgroundImage")));
            this.pictureBox_image.Location = new System.Drawing.Point(6, 20);
            this.pictureBox_image.Name = "pictureBox_image";
            this.pictureBox_image.Size = new System.Drawing.Size(542, 511);
            this.pictureBox_image.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_image.TabIndex = 0;
            this.pictureBox_image.TabStop = false;
            // 
            // groupBox_tools_item
            // 
            this.groupBox_tools_item.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox_tools_item.Location = new System.Drawing.Point(12, 130);
            this.groupBox_tools_item.Name = "groupBox_tools_item";
            this.groupBox_tools_item.Size = new System.Drawing.Size(200, 419);
            this.groupBox_tools_item.TabIndex = 1;
            this.groupBox_tools_item.TabStop = false;
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.groupBox_tools_item);
            this.Controls.Add(this.groupBox_pic);
            this.Controls.Add(this.groupBox_tool);
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "Form_Main";
            this.Text = "ImageCompress";
            this.groupBox_tool.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.groupBox_pic.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_image)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox_tool;
        private System.Windows.Forms.GroupBox groupBox_pic;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button button_open;
        private System.Windows.Forms.ComboBox comboBox_tools;
        private System.Windows.Forms.Button button_save;
        private System.Windows.Forms.GroupBox groupBox_tools_item;
        public System.Windows.Forms.PictureBox pictureBox_image;
    }
}

