﻿using ImageCompress;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageHandle
{
    public partial class Form_Main : Form
    {
        public Form_Main()
        {
            InitializeComponent();

            InitData();
        }

        Dictionary<string, Action> name_to_func = new Dictionary<string, Action>();

        private void InitData()
        {
            string drop_items = "ImageZoom";

            foreach (var item in drop_items.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                comboBox_tools.Items.Add(item);
            }
        }

        private string SavePath;

        private void button_open_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "图片文件|*.jpg";
            var dr = ofd.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                if (File.Exists(ofd.FileName))
                {
                    pictureBox_image.Image = Image.FromFile(ofd.FileName);
                    SavePath = null;
                }
            }
        }

        private void button_save_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(SavePath))
            {
                SaveFileDialog sfd = new SaveFileDialog();
                var dr = sfd.ShowDialog();
                if (dr == System.Windows.Forms.DialogResult.OK)
                {
                    SavePath = sfd.FileName;
                }
            }

            if (!string.IsNullOrEmpty(SavePath))
            {
                ImageHelper.GetInstance().SaveImage(new Bitmap(pictureBox_image.Image), SavePath, ImageFormat.Jpeg, 100);
            }
        }

        private void comboBox_tools_SelectedIndexChanged(object sender, EventArgs e)
        {
            var select_name = comboBox_tools.SelectedItem.ToString();

            groupBox_tools_item.Controls.Clear();

            if (name_to_func.ContainsKey(select_name))
            {
                name_to_func[select_name]();
            }
            else
            {
                var item = Assembly.GetExecutingAssembly().CreateInstance(string.Format("ImageHandle.ImageControl.{0}", select_name));
                if (item != null && item is Control)
                {
                    var control = item as Control;
                    groupBox_tools_item.Controls.Add(control);

                    control.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
                    control.Location = new Point(10, 10);
                    control.Size = groupBox_tools_item.Size + new Size(-20, -20);

                    var pro = control.GetType().GetProperty("FormMain");
                    pro.SetValue(control, this);
                }
            }
        }
    }
}
