﻿namespace ImageHandle.ImageControl
{
    partial class ImageZoom
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label_ImageZoom = new System.Windows.Forms.Label();
            this.comboBox_ImageZoom = new System.Windows.Forms.ComboBox();
            this.button_run = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label_ImageZoom
            // 
            this.label_ImageZoom.Location = new System.Drawing.Point(5, 10);
            this.label_ImageZoom.Name = "label_ImageZoom";
            this.label_ImageZoom.Size = new System.Drawing.Size(256, 23);
            this.label_ImageZoom.TabIndex = 0;
            this.label_ImageZoom.Text = "ImageZoom";
            this.label_ImageZoom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBox_ImageZoom
            // 
            this.comboBox_ImageZoom.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_ImageZoom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_ImageZoom.FormattingEnabled = true;
            this.comboBox_ImageZoom.Location = new System.Drawing.Point(5, 36);
            this.comboBox_ImageZoom.Name = "comboBox_ImageZoom";
            this.comboBox_ImageZoom.Size = new System.Drawing.Size(256, 20);
            this.comboBox_ImageZoom.TabIndex = 1;
            // 
            // button_run
            // 
            this.button_run.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button_run.Location = new System.Drawing.Point(5, 62);
            this.button_run.Name = "button_run";
            this.button_run.Size = new System.Drawing.Size(258, 23);
            this.button_run.TabIndex = 2;
            this.button_run.Text = "Run";
            this.button_run.UseVisualStyleBackColor = true;
            this.button_run.Click += new System.EventHandler(this.button_run_Click);
            // 
            // ImageZoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button_run);
            this.Controls.Add(this.comboBox_ImageZoom);
            this.Controls.Add(this.label_ImageZoom);
            this.Name = "ImageZoom";
            this.Size = new System.Drawing.Size(266, 150);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label_ImageZoom;
        private System.Windows.Forms.ComboBox comboBox_ImageZoom;
        private System.Windows.Forms.Button button_run;

    }
}
