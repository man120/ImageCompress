﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ImageCompress;

namespace ImageHandle.ImageControl
{
    public partial class ImageZoom : UserControl
    {
        public ImageZoom()
        {
            InitializeComponent();

            foreach (var item in Enum.GetValues(typeof(ImageZoomType)))
            {
                comboBox_ImageZoom.Items.Add(item.ToString());
            }

            comboBox_ImageZoom.SelectedIndex = 0;
        }

        public Form_Main FormMain { get; set; }

        private void button_run_Click(object sender, EventArgs e)
        {
            Bitmap source = new Bitmap(FormMain.pictureBox_image.Image);
            Bitmap output = new Bitmap(FormMain.pictureBox_image.Width, FormMain.pictureBox_image.Height);
            ImageHelper.GetInstance().ImageZoom(source, output, (ImageZoomType)Enum.Parse(typeof(ImageZoomType), comboBox_ImageZoom.SelectedItem.ToString()));

            FormMain.pictureBox_image.Image = output;
        }
    }
}
